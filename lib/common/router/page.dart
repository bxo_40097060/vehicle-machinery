import 'package:flutter_hmi/common/constant/router_name.dart';
import 'package:flutter_hmi/pages/application/binding.dart';
import 'package:flutter_hmi/pages/application/pages/contacts/binding.dart';
import 'package:flutter_hmi/pages/application/pages/find/view.dart';
import 'package:flutter_hmi/pages/application/pages/message/binding.dart';
import 'package:flutter_hmi/pages/application/pages/user/binding.dart';
import 'package:flutter_hmi/pages/application/pages/user/view.dart';
import 'package:flutter_hmi/pages/application/view.dart';
import 'package:flutter_hmi/pages/chat/chat_user/binding.dart';
import 'package:flutter_hmi/pages/chat/chat_user/view.dart';
import 'package:flutter_hmi/pages/login/binding.dart';
import 'package:flutter_hmi/pages/login/view.dart';
import 'package:flutter_hmi/pages/register/register.dart';
import 'package:get/get.dart';

class AppPages {
  // 初始页面
  static const String initialRoute = RouterName.index;

  // 定义路由
  static final List<GetPage<dynamic>> pages = [
    // 允许匿名
    GetPage(
      name: "/login",
      page: () => const LoginPage(),
      binding: LoginBinding(),
    ),

    // 需要登录
    GetPage(
      name: RouterName.index,
      page: () => const ApplicationPage(),
      bindings: [
        ApplicationBinding(),
        MessageBinding(),
        ContactsBinding(),
        UserBinding(),
      ],
    ),
    GetPage(
      name: RouterName.chatUser,
      page: () => ChatUserPage(),
      binding: ChatUserBinding(),
    ),
    GetPage(
      name: RouterName.chatUser,
      page: () => ChatUserPage(),
      binding: ChatUserBinding(),
    ),
    GetPage(
      name: RouterName.find,
      // ignore: prefer_const_constructors
      page: () => FindState(),
      binding: ChatUserBinding(),
    ),
    GetPage(
      name: RouterName.mine,
      page: () => const UserPage(),
      binding: UserBinding(),
    ),
    GetPage(
      name: RouterName.register,
      page: () => const RegisterPage(),
      binding: UserBinding(),
    ),
  ];
}
