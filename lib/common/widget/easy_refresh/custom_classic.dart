import 'package:easy_refresh/easy_refresh.dart';

class CustomClassic {
  ClassicHeader getHeader() {
    return const ClassicHeader(
      dragText: '下拉刷新',
      armedText: '释放刷新',
      readyText: '加载中...',
      processingText: '加载中...',
      processedText: '加载完成',
      noMoreText: '没有更多',
      failedText: '加载失败',
      messageText: '最后更新于 %T',
    );
  }

  ClassicFooter getFooter() {
    return const ClassicFooter(
      dragText: '上拉加载',
      armedText: '释放刷新',
      readyText: '加载中...',
      processingText: '加载中...',
      processedText: '加载完成',
      noMoreText: '没有更多',
      failedText: '加载失败',
      messageText: '最后更新于 %T',
      // showMessage: false, // 隐藏更新时间
      position: IndicatorPosition.above,
      infiniteOffset: null,
    );
  }
}
