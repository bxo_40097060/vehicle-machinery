import 'package:flutter/material.dart';

import '../../style/theme.dart';

class CellWidget extends StatelessWidget {
  final IconData icon;
  final String title;
  final Widget right;
  final Function? onTap;
  const CellWidget({
    super.key,
    this.onTap,
    required this.right,
    required this.title,
    required this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) {
          onTap!();
        }
      },
      child: Container(
        color: Colors.white,
        padding: const EdgeInsets.all(AppTheme.largeSpace),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Icon(
                  icon,
                  color: AppTheme.primaryColor,
                  size: 20,
                ),
                const SizedBox(width: AppTheme.largeSpace),
                Text(title),
              ],
            ),
            right,
          ],
        ),
      ),
    );
  }
}
