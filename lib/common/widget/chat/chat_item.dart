import 'package:flutter/material.dart';

import '../../style/theme.dart';
import '../image/image.dart';

class ChatItem extends StatelessWidget {
  const ChatItem({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: ListTile(
        dense: true,
        contentPadding: EdgeInsets.symmetric(
          vertical: 1,
          horizontal: AppTheme.largeSpace,
        ),
        horizontalTitleGap: AppTheme.largeSpace,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "闲谈",
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: AppTheme.middleFont,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              "12:19",
              style: TextStyle(
                color: AppTheme.infoColor,
              ),
            )
          ],
        ),
        subtitle: Text(
          "[视频]分享一个免费的AI编程小助手",
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: AppTheme.infoColor,
          ),
        ),
        leading: ImageWidget(
          path: "http://q1.qlogo.cn/g?b=qq&nk=2327972001&s=640",
          size: 45,
          radius: AppTheme.radius,
        ),
      ),
    );
  }
}
