import 'package:chat_bubbles/chat_bubbles.dart';
import 'package:flutter/material.dart';

import '../../style/theme.dart';
import '../image/image.dart';

class ChatLog extends StatefulWidget {
  final List<Widget> name;
  final String avatar;
  final String content;
  final bool isSender;
  const ChatLog({
    super.key,
    this.isSender = false,
    required this.name,
    required this.avatar,
    required this.content,
  });

  @override
  State<ChatLog> createState() => _ChatLogState();
}

class _ChatLogState extends State<ChatLog> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment:
          widget.isSender ? MainAxisAlignment.end : MainAxisAlignment.start,
      children: [
        Visibility(
          visible: !widget.isSender,
          child: ImageWidget(
            path: widget.avatar,
            size: 50,
            radius: 100,
          ),
        ),
        Column(
          crossAxisAlignment: widget.isSender
              ? CrossAxisAlignment.end
              : CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.only(
                left: AppTheme.middleSpace,
                right: AppTheme.middleSpace,
              ),
              child: Wrap(
                spacing: AppTheme.smallSpace,
                crossAxisAlignment: WrapCrossAlignment.center,
                children: widget.name,
              ),
            ),
            const SizedBox(height: AppTheme.middleSpace),
            BubbleSpecialOne(
              text: widget.content,
              isSender: widget.isSender,
              color: widget.isSender ? const Color(0xff95EC69) : Colors.white,
              textStyle: TextStyle(
                color: widget.isSender ? const Color(0xff232323) : null,
                fontSize: AppTheme.middleFont,
              ),
            ),
          ],
        ),
        Visibility(
          visible: widget.isSender,
          child: ImageWidget(
            path: widget.avatar,
            size: 50,
            radius: 100,
          ),
        ),
      ],
    );
  }
}
