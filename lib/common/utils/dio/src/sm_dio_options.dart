import 'package:dio/dio.dart';

const BaseUrl = 'https://mock.apifox.com/m1/2802142-0-default';

/// BaseOptions
class SMDioOptions {
  // 默认配置
  static BaseOptions defaultOption({required String baseUrl}) => BaseOptions(
      baseUrl: baseUrl,
      sendTimeout: const Duration(milliseconds: 5000),
      receiveTimeout: const Duration(milliseconds: 5000),
      responseType: ResponseType.json);

  // 自定义配置
  static BaseOptions customOption({
    String baseUrl = '',
    int connectTimeout = 5000,
    int receiveTimeout = 5000,
    int sendTimeout = 5000,
    Map<String, dynamic>? headers,
    ResponseType? responseType = ResponseType.json,
    String? contentType,
  }) {
    return BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: Duration(milliseconds: connectTimeout),
      receiveTimeout: Duration(milliseconds: receiveTimeout),
      sendTimeout: Duration(milliseconds: sendTimeout),
      headers: headers,
      contentType: contentType,
      responseType: responseType,
    );
  }
}
