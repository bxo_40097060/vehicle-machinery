import 'package:flutter/material.dart';

class AppTheme {
  // 间距
  static const double smallSpace = 2.5;
  static const double middleSpace = 5.0;
  static const double largeSpace = 10.0;

  // 字体大小
  static const double smallFont = 12;
  static const double middleFont = 14;
  static const double largeFont = 16;

  // 主配色
  static const Color primaryColor = Colors.blue;
  static const Color infoColor = Color(0xff6B6B6B);
  static const Color warningColor = Color(0xffE6A23C);
  static const Color dangerColor = Color(0xffF56C6C);

  // 圆角
  static const double radius = 5;
  static const double round = 100;
}
