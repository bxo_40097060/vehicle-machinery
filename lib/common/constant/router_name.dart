import 'package:flutter_hmi/common/constant/router_param.dart';

class RouterName {
  // 首页
  static const String index = "/";

  // 聊天页面
  static const String chat = "/chat";
  static const String chatUser = "$chat/user/:${RouterParam.username}";
  static const String find = "/find";
  static const String mine = "/mine";
  static const String register = "/register";
}
