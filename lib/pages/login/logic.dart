import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import 'state.dart';

class LoginLogic extends GetxController {
  final LoginState state = LoginState();
  final formKey = GlobalKey<FormState>();

  updateLoginData(String field, String value) {
    state.loginData = {
      ...state.loginData,
      field: value,
    };
  }
}
