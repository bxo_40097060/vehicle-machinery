import 'package:get/get.dart';

class LoginState {
  final _loginData = {}.obs;
  set loginData(value) => _loginData.value = value;
  Map get loginData => {..._loginData};
  LoginState() {
    ///Initialize variables
  }
}
