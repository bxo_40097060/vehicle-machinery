import 'package:flutter/material.dart';
import 'package:flutter_hmi/common/utils/random.dart';
import 'package:get/get.dart';
import 'package:scrollview_observer/scrollview_observer.dart';

import 'state.dart';

class ChatUserLogic extends GetxController {
  final ChatUserState state = ChatUserState();
  final scrollController = ScrollController();
  late ListObserverController observerController;
  late ChatScrollObserver chatObserver;

  @override
  void onInit() {
    super.onInit();
    observerController = ListObserverController(controller: scrollController)
      ..cacheJumpIndexOffset = false;
    chatObserver = ChatScrollObserver(observerController)
      ..toRebuildScrollViewCallback = () {
        // 这里可以重建指定的滚动视图即可
      };
    chatObserver.standby();
  }

  @override
  void onReady() {
    super.onReady();
    state.chatLogList = [
      {
        "avatar": "http://q1.qlogo.cn/g?b=qq&nk=2327972001&s=640",
        "name": "小明",
        "title": "明恒网络工作室",
        "content": "哈哈哈，你好呀！",
      }
    ];
  }

  addChatLog() {
    chatObserver.standby(changeCount: 1);
    var chatLogList = state.chatLogList;
    var qq = RandomUtils.genInt(min: 2311111111, max: 2399999999);
    chatLogList.insert(0, {
      "avatar": "http://q1.qlogo.cn/g?b=qq&nk=$qq&s=640",
      "name": "小明$qq",
      "title": "明恒网络工作室",
      "content": "哈哈哈，你好呀！",
    });
    state.chatLogList = chatLogList;
  }
}
