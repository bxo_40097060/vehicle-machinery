import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hmi/common/constant/router_param.dart';
import 'package:get/get.dart';
import 'package:scrollview_observer/scrollview_observer.dart';

import '../../../common/constant/router_tag.dart';
import '../../../common/style/theme.dart';
import '../../../common/widget/app_bar/app_bar.dart';
import '../../../common/widget/chat/chat_log.dart';
import '../../../common/widget/easy_refresh/custom_classic.dart';
import 'logic.dart';

class ChatUserPage extends GetView<ChatUserLogic> {
  ChatUserPage({Key? key}) : super(key: key);

  @override
  final String? tag =
      "${RouterTag.chatUser}${Get.parameters[RouterParam.username]}";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar(
        "小明",
        actions: _buildActions(),
      ),
      body: _buildBody(),
    );
  }

  List<Widget> _buildActions() {
    return [
      IconButton(
        onPressed: controller.addChatLog,
        icon: const Icon(
          Icons.add_circle_outline,
        ),
      ),
    ];
  }

  Widget _buildBody() {
    Widget resultWidget = Column(
      children: [
        Expanded(child: _buildListView()),
      ],
    );

    resultWidget = Container(
      padding: const EdgeInsets.all(
        AppTheme.largeSpace,
      ),
      child: resultWidget,
    );

    return resultWidget;
  }

  Widget _buildListView() {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        Widget resultWidget = EasyRefresh.builder(
          header: CustomClassic().getHeader(),
          footer: CustomClassic().getFooter(),
          onRefresh: () async {
            await Future.delayed(const Duration(seconds: 2));
          },
          onLoad: () async {
            await Future.delayed(const Duration(seconds: 2));
          },
          childBuilder: (context, physics) {
            var scrollViewPhysics = physics.applyTo(
              ChatObserverClampingScrollPhysics(
                observer: controller.chatObserver,
              ),
            );
            Widget resultWidget = Obx(() {
              return ListView.separated(
                physics: controller.chatObserver.isShrinkWrap
                    ? const NeverScrollableScrollPhysics()
                    : scrollViewPhysics,
                shrinkWrap: controller.chatObserver.isShrinkWrap,
                reverse: true,
                controller: controller.scrollController,
                itemCount: controller.state.chatLogList.length,
                itemBuilder: ((context, index) {
                  var chatLog = controller.state.chatLogList[index];
                  return ChatLog(
                    isSender: chatLog["name"] == "小明",
                    name: [
                      Text(
                        "${chatLog["name"]}",
                        style: const TextStyle(
                          fontSize: AppTheme.smallFont,
                        ),
                      ),
                      Text(
                        "@${chatLog["title"]}",
                        style: const TextStyle(
                          color: Colors.orangeAccent,
                          fontSize: AppTheme.smallFont,
                        ),
                      ),
                    ],
                    avatar: "${chatLog["avatar"]}",
                    content: "${chatLog["content"]}",
                  );
                }),
                separatorBuilder: (BuildContext context, int index) {
                  return const SizedBox(
                    height: AppTheme.largeSpace,
                  );
                },
              );
            });

            if (controller.chatObserver.isShrinkWrap) {
              resultWidget = SingleChildScrollView(
                reverse: true,
                physics: scrollViewPhysics,
                child: Container(
                  alignment: Alignment.topCenter,
                  height: constraints.maxHeight + 0.001,
                  child: resultWidget,
                ),
              );
            }

            return resultWidget;
          },
        );

        resultWidget = ListViewObserver(
          controller: controller.observerController,
          child: resultWidget,
        );

        resultWidget = Align(
          alignment: Alignment.topCenter,
          child: resultWidget,
        );

        return resultWidget;
      },
    );
  }
}
