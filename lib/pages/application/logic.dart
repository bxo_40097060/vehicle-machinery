import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'state.dart';

class ApplicationLogic extends GetxController {
  final ApplicationState state = ApplicationState();
  final pageController = PageController(initialPage: 0);

  onClickNavBar(int index) {
    state.selectedPageIndex = index;
    pageController.animateToPage(
      index,
      duration: const Duration(milliseconds: 200),
      curve: Curves.ease,
    );
  }
}
