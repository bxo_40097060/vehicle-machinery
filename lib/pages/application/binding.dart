import 'package:flutter_hmi/pages/application/pages/contacts/logic.dart';
import 'package:flutter_hmi/pages/application/pages/message/logic.dart';
import 'package:flutter_hmi/pages/application/pages/user/logic.dart';
import 'package:get/get.dart';

import 'logic.dart';

class ApplicationBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ApplicationLogic());
  }
}
