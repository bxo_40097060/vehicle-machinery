import 'package:get/get.dart';

import 'logic.dart';

class ContactsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ContactsLogic());
  }
}
