import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hmi/common/widget/easy_refresh/custom_classic.dart';
import 'package:flutter_hmi/common/widget/image/image.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import '../../../../common/widget/chat/chat_item.dart';
import 'logic.dart';

import 'dart:ui';

class MessagePage extends StatefulWidget {
  const MessagePage({super.key});

  @override
  State<StatefulWidget> createState() => _MessagePageState();
}

class _MessagePageState extends State<MessagePage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
          height: MediaQuery.of(context).size.height,
          color: Colors.black,
          child: Stack(
            textDirection: TextDirection.ltr,
            fit: StackFit.loose,
            alignment: Alignment.center,
            children: [
              LottieBuilder.network(
                "https://lottie.host/957b4c72-4ad1-412b-a759-5fba7329b19f/2PeaybPSmL.json",
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                animate: true,
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                        width: 300,
                        height: 300,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: Colors.transparent,
                        ),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            LottieBuilder.network(
                              "https://lottie.host/e8dfc441-29b0-475e-bbc9-67161ede0137/eHzj0ibZSG.json",
                              width: 300,
                              height: 300,
                            ),
                            const Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "60",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 60,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  "kill",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ],
                            )
                          ],
                        )),
                    Container(
                      color: Colors.transparent,
                      width: 100,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.arrow_back_ios,
                                color: Colors.white,
                              ),
                              Text(
                                "N",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              ),
                              Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 100,
                            height: 200,
                            child: LottieBuilder.network(
                              "https://lottie.host/780203cf-826b-41e4-8a22-58c467d515d3/COQDl74NdK.json",
                              width: 100,
                              height: 200,
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                        width: 300,
                        height: 300,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: Colors.transparent,
                        ),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            LottieBuilder.network(
                              "https://lottie.host/e8dfc441-29b0-475e-bbc9-67161ede0137/eHzj0ibZSG.json",
                              width: 300,
                              height: 300,
                            ),
                            const Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "80",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 60,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  "km",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ],
                            )
                          ],
                        ))
                  ]),
            ],
          )),
    );
  }
}
