import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:scrollview_observer/scrollview_observer.dart';

import 'state.dart';

class MessageLogic extends GetxController {
  final MessageState state = MessageState();
}
