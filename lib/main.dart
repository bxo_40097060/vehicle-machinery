import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hmi/common/router/page.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:amap_flutter_map/amap_flutter_map.dart';

final easyLoading = EasyLoading.init();

/*
title：应用标题；
debugShowCheckedModeBanner：是否显示debug标志；
theme：应用主题，包括颜色、字体等；
home：应用启动后的首页，可以是一个页面组件，也可以是GetxController实例；
initialRoute：应用启动时的初始路由；
getPages：路由表，用来配置应用的路由信息；
defaultTransition：默认的路由过渡效果；
onInit：GetxController的初始化逻辑，包括初始化数据、绑定事件等；
onReady：GetxController准备完成时的逻辑；
onClose：GetxController销毁时的逻辑；
*/

void main() {
  runApp(
    GetMaterialApp(
      debugShowCheckedModeBanner: false,
      builder: (context, widget) {
        widget = easyLoading(context, widget);
        return widget;
      },
      theme: ThemeData(
        scaffoldBackgroundColor: const Color(0xffF5F5F5),
      ),
      getPages: AppPages.pages,
      initialRoute: AppPages.initialRoute,
    ),
  );
}

class AmapConfig {
  static const amapAndroidKey = "";
  static const amapIosKey = "";
}
